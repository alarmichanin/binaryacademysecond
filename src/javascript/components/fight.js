import { controls } from '../../constants/controls';
import { toggleShield, showAttack, changeHP } from './fightersView';
// import { changeHealthbarWidth, showAttack, toggleShield } from './fightersView';
import Fighter from './fighter';

let start
export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let winnerPlayer = ''
    const btnSet = new Set()
    const players = createFighters(firstFighter, secondFighter)

    const onKeyDown = (e) => {
      handleKeyDown(e, btnSet, players)
      winnerPlayer = endGame(players)
      if (winnerPlayer) {
        winnerPlayer === 1 && resolve(firstFighter)
        winnerPlayer === 2 && resolve(secondFighter)
        document.removeEventListener('keydown', onKeyDown)
        document.removeEventListener('keyup', onKeyUp)
      }
    }
    const onKeyUp = (e) => handleKeyUp(e, btnSet, players)
    document.addEventListener('keydown', onKeyDown)
    document.addEventListener('keyup', onKeyUp)
  });
}

function endGame({ firstFighter, secondFighter }) {
  if (!firstFighter.health)
    return 2
  if (!secondFighter.health)
    return 1
  return ''
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender)
  if (damage >= 0)
    return damage
  return 0

}

export function getHitPower(fighter) {
  const criticalHit = Math.random() + 1
  return fighter.attack * criticalHit
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1
  return fighter.defense * dodgeChance
}

function getUltraAttackPower(fighter) {
  return fighter.attack * 2
}

function createFighters(firstFighter, secondFighter) {
  return {
    firstFighter: new Fighter(1, firstFighter),
    secondFighter: new Fighter(2, secondFighter)
  }
}

function handleKeyDown(e, btnSet, { firstFighter, secondFighter }) {
  if (btnSet.has(e.code)) return
  btnSet.add(e.code)
  if (e.code === controls.PlayerOneBlock) {
    return toggleShield('left')
  }
  if (e.code === controls.PlayerTwoBlock) {
    return toggleShield('right')
  }
  fightAction(btnSet, { firstFighter, secondFighter })
}

function handleKeyUp(e, btnSet, { firstFighter, secondFighter }) {
  btnSet.delete(e.code)
  switch (e.code) {
    case controls.PlayerOneAttack:
      return (firstFighter.canAttack = true)
    case controls.PlayerTwoAttack:
      return (secondFighter.canAttack = true)
    case controls.PlayerOneBlock:
      return toggleShield('left')
    case controls.PlayerTwoBlock:
      return toggleShield('right')
  }
}

function fightAction(btnSet, { firstFighter, secondFighter }) {
  switch (true) {
    case doUltraAttack(
      btnSet,
      controls.PlayerOneCriticalHitCombination,
      controls.PlayerOneBlock,
      firstFighter.canUltraAttack
    ):
      return effectUltraAttack(firstFighter, secondFighter)
    case doUltraAttack(
      btnSet,
      controls.PlayerTwoCriticalHitCombination,
      controls.PlayerTwoBlock,
      secondFighter.canUltraAttack
    ):
      return effectUltraAttack(secondFighter, firstFighter)
    case doAttack(btnSet, controls.PlayerOneAttack, controls.PlayerOneBlock, firstFighter.canAttack):
      return effectAttack(firstFighter, secondFighter, btnSet, controls.PlayerTwoBlock)
    case doAttack(btnSet, controls.PlayerTwoAttack, controls.PlayerTwoBlock, secondFighter.canAttack):
      return effectAttack(secondFighter, firstFighter, btnSet, controls.PlayerOneBlock)
    default:
      return
  }
}

function doAttack(btnSet, controlAttack, controlBlock, canAttack) {
  return btnSet.has(controlAttack) && !btnSet.has(controlBlock) && canAttack;
}

function effectAttack(attacker, defender, btnSet, controlDefense) {
  const positionAttacker = attacker.num === 1 ? 'left' : 'right'
  showAttack(positionAttacker, 'hit')
  attacker.canAttack = false
  const damage = btnSet.has(controlDefense) ? getDamage(attacker, defender) : getHitPower(attacker)
  defender.decreaseHealth(damage)
  const positionDefender = defender.num === 1 ? 'left' : 'right'
  changeHP(defender, positionDefender)
}

export function doUltraAttack(btnSet, controlAttack, controlBlock, canUltraAttack) {
  start = 0
  return checkUltraAttack(btnSet, controlAttack) && !btnSet.has(controlBlock) && canUltraAttack
}

function checkUltraAttack(btnSet, control) {
  if (btnSet < control) {
    return false
  }
  return controlValuesInSet(btnSet, control)
}

function controlValuesInSet(set, control) {
  const len = control.length
  let result = true
  for (let i = 0; i < len; i++) {
    if (!set.has(control[i])) {
      result = false
      break
    }
  }
  return result
}

function effectUltraAttack(attacker, defender) {
  const positionAttacker = attacker.num === 1 ? 'left' : 'right'
  const positionDefender = defender.num === 1 ? 'left' : 'right'
  showAttack(positionAttacker, 'ultra')
  defender.decreaseHealth(getUltraAttackPower(attacker))
  changeHP(defender, positionDefender)
  attacker.canUltraAttack = false
  ultraProgress(attacker, positionAttacker)
}

export function ultraProgress(player, position) {
  let start = 0
  const ultraIntervalId = setInterval(function () {
    const indicator = document.getElementById(`arena__ultraindicator-progress-${position}`)
    if (start > 11) {
      clearInterval(ultraIntervalId)
    } else if (start === 11) {
      activateUltra(player, position)
    } else if (start === 0) {
      const ultraindicator = document.getElementById(`${position}-ultraindicator`)
      ultraindicator.style.visibility = 'hidden'
    } else {
      indicator.value = start
    }
    start++
  }, 1000)
}

function activateUltra(player, position) {
  const ultraindicator = document.getElementById(`${position}-ultraindicator`)
  ultraindicator.style.visibility = 'visible'
  player.canUltraAttack = true
}

