import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const imgFighter = createFighterImage(fighter);
  showModal({
    title: `${fighter.name} WON!!!`,
    bodyElement: imgFighter,
    onClose: () => {
      location.reload();
    }
  });
}