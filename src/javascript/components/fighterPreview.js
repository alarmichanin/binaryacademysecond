import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___block'
  })
  const fighterName = createElement({
    tagName: 'h1',
    className: 'fighter-preview___name'
  })
  const fighterData = createFighterData(fighter)
  fighterName.innerText = `NAME: ${fighter.name}`;
  fighterInfo.append(fighterName, fighterData)
  fighterElement.append(fighterImage, fighterInfo)

  return fighterElement;
}

function createFighterData(fighter) {
  const fighterData = createElement({
    tagName: 'div',
    className: 'fighter-preview___labels'
  });
  const healthIndicator = createFighterDataIndicator('health', fighter.health);
  const attackIndicator = createFighterDataIndicator('attack', fighter.attack);
  const defenseIndicator = createFighterDataIndicator('protection', fighter.defense);
  fighterData.append(healthIndicator, attackIndicator, defenseIndicator);
  return fighterData;
}

function createFighterDataIndicator(characteristic, value) {
  const indicator = createElement({
    tagName: 'div',
    className: 'fighter-preview___indicator'
  });
  const valueElement = createElement({
    tagName: 'h2'
  });
  valueElement.innerText = `${characteristic.toUpperCase()}: ${value}`;
  indicator.append(valueElement);
  return indicator;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
