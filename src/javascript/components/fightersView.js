import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';

export function createFighters(fighters) {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter, selectFighter) {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = (event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}
export function toggleShield(position) {
  const shield = document.getElementById(`${position}-shield`);
  shield.style.visibility = shield.style.visibility === 'visible' ? 'hidden' : 'visible';
}


export function showAttack(position, attack) {
  const attackShow = document.getElementById(`${position}-${attack}`);
  attackShow.classList.add(`arena___${position}-${attack}-show`);
  setTimeout(() => {
    attackShow.classList.remove(`arena___${position}-${attack}-show`);
  }, 150);
}
export function changeHP({ initialHealth, health }, position) {
  const startWidth = document.getElementById('left-fighter-indicator').offsetWidth;
  const hpBar = document.getElementById(`${position}-fighter-indicator`);
  const newHPbar = (startWidth * health) / initialHealth;
  const hpWidth = newHPbar >= 0 ? newHPbar : 0;
  hpBar.style.width = `${hpWidth}px`;
}